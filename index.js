"use strict";
const fs = require('fs');
const keyFile  = fs.readFileSync(__dirname + '/key.pem');
const certFile = fs.readFileSync(__dirname + '/a466b08245781429.crt');
module.exports = {
    keyFile,
    certFile
}